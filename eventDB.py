from urllib2 import urlopen
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
import sys
import json
import re

TARGET_URL = 'http://studentevents.ucsd.edu/all'

def soup_html_structure(web_url):
	# generates the html structure
	# of the website were looking at
	html = urlopen(web_url).read()
	# Turn the soup looking structure
	# in to nice HTML looking structure
	return BeautifulSoup(html, 'lxml')

def timeStamp(time, date):
	try:
		timeItem = re.split(' ', time)
		timeTK = ''
		timeTKe = ''

		#
		#	Parsing the time tokens
		# - start timestamp
		if 'PM' in timeItem[0]:
			hourTK = re.split(':', timeItem[0])
			minTK = re.split('PM', hourTK[1])[0]
			if hourTK[0] == "12":
				timeTK = hourTK[0] + ":" + minTK + ":00"
			else:
				hourTK = str(int(hourTK[0]) + 12)
				timeTK = hourTK + ":" + minTK + ":00"
		else:
			if int(re.split(':', re.split('AM', timeItem[0])[0])[0]) < 10:
				timeTK = '0' + re.split('AM', timeItem[0])[0] + ':00'
			else:
				timeTK = re.split('AM', timeItem[0])[0] + ':00'

		#	- end timestamp
		if 'PM' in timeItem[2]:
			hourTK = re.split(':', timeItem[2])
			minTK = re.split('PM', hourTK[1])[0]
			if hourTK[0] == "12":
				timeTKe = hourTK[0] + ":" + minTK + ":00"
			else:
				hourTK = str(int(hourTK[0]) + 12)
				timeTKe = hourTK + ":" + minTK + ":00"
		else:
			if int(re.split(':', re.split('AM', timeItem[0])[0])[0]) < 10:
				timeTKe = '0' + re.split('AM', timeItem[0])[0] + ':00'
			else:
				timeTKe = re.split('AM', timeItem[0])[0] + ':00'

		#
		#	Parsing the date tokens
		#
		dateItem = re.split(' ', date)
		monthItem = {
			'Jan': 1,
			'Feb': 2,
			'Mar': 3,
			'Apr': 4,
			'May': 5,
			'Jun': 6,
			'Jul': 7,
			'Aug': 8,
			'Sep': 9,
			'Oct': 10,
			'Nov': 11,
			'Dec': 12
		}

		dateTok = dateItem[0] + '-' + str(monthItem[dateItem[1]]) + '-' + dateItem[2]
		timeStampStart = dateTok + ' ' + timeTK
		timeStampEnd = dateTok + ' ' + timeTKe
		return [timeStampStart, timeStampEnd]
	except Exception:
		return ['none', 'none']

def get_events(section_url):
	soup = soup_html_structure(section_url)
	# HTML structure for events found in ul
	eventHTML = soup.find('ul', 'page_listing vert-list')
	
	# Organlize the HTML structure into separate nodes
	events = [li for li in eventHTML.find_all('li')]

	eventList = []	

	for event in events:

		# extract event information
		div = event.find('div', 'art-text')
		# extract date infromation
		div_date = event.find('div', 'art-date')

		# clean the html strucutre of any </br>	
		try:		
			for e in div.findAll('br'):
				e.extract()
		except: 
			continue;

		title=""
		location=""
		time=""
		month=""
		day=""
		date=""
		img_src=""
		# event information extraction
		if div.find('h1').string is not None:
			title = div.find('h1').string
		if div.find('h2').string is not None:
			location=div.find('h2').string
		if div.findAll('h2')[1].string is not None:
			time = div.findAll('h2')[1].string
		if event.find('img')['src'] is not None:
			img_src = event.find('img')['src']
		# event date extraction
		if div_date.find('span', 'main-art-month').string is not None:
			month = div_date.find('span', 'main-art-month').string
		if div_date.find('span', 'main-art-date').string is not None:
			day = div_date.find('span', 'main-art-date').string
		if month is not None and day is not None:
			date = '2015' + ' ' + month + ' ' + day
		# generate the link that will give us the description
		href = event.find('a')['href']

		# filter out non-events
		if timeStamp(time,date)[0] is 'none':
			continue

		event_info = {
			"title": title,
			"location": location,
			"time": time,
			"month": month,
			"date": date,
			"href":  href,
			"timeStampStart": timeStamp(time, date)[0],
			"timeStampEnd": timeStamp(time, date)[1],
			"img_src": img_src,
			"category": 'example'
		}


		print event_info
		eventList.append(event_info)

	return eventList

def add_description(eventArray):
	url = TARGET_URL + '/'

	for event in eventArray:
		browser = webdriver.Firefox()
		browser.get(url + event['href'])

		delay = 30 # seconds
		try:
			WebDriverWait(browser, delay).until(EC.presence_of_element_located((By.CLASS_NAME, "description")))
			html = browser.page_source
			htmlSoup = BeautifulSoup(html, 'lxml')
			jsModal = htmlSoup.find('div', 'ui-dialog-content')
			
			for e in jsModal.findAll('br'):
				e.extract()
			
			description = jsModal.find('p', 'description').get_text(' ', strip=True)

			del(event['href'])
			event['description'] = ""
			if description is not None:
				event['description'] = description

			browser.quit()
		except TimeoutException:
		    print "Loading took too much time!"
	return eventArray

def stripUnicode(jsonList):
	for dict in jsonList:
		for k in dict:
			dict[k]=dict[k].encode('ascii','ignore')


events = get_events(TARGET_URL)
add_description(events)
stripUnicode(events);

orig_stdout = sys.stdout
f = file('events.json', 'w')
sys.stdout = f

print json.dumps(events)

sys.stdout = orig_stdout
f.close()

# STATUS 001: I don't know how to handle 
#			  the <br> tags in the html 
#             structure